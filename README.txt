TestIterator Questions:

    "TODO also try with a LinkedList - does it make any difference?"

    Yes, although not on the surface area as it they both function similarly. However, after running tests, there is a slight increase in performance.

    "TODO what happens if you use list.remove(Integer.valueOf(77))?"

    The program will check the current value in the list and only remove it if it matches the value (77).

TestList Questions:

    "TODO also try with a LinkedList - does it make any difference?"

    Yes, although not on the surface area as it they both function similarly. However, after running tests, there is a slight increase in performance.

    "list.remove(5); - what does this method do?"

    This method will remove the value at index 5 (the sixth index). After it is removed, all values after it will shift their indexs accordingly.

    "list.remove(Integer.valueOf(5)); - what does this one do?"

    This method will remove the value (5) from its index. After it is removed, all values after it will shift their indexs accordingly.

TestPerformance Questions:

    Which of the two lists performs better as the size increases?

    Overall, the LinkedList performs better. This is more notacable as the size increases up to 10,000. At this point the performance os more notable.

TestPerformance Results:

        testRemove (ArrayList)	testRemove (LinkedList)	testAverageValues (ArrayList)	testAverageValues (LinkedList)	testSubList (ArrayList)	testSubList (LinkedList)	testSet (ArrayList)	testSet (LinkedList)
10    |      0.017	                       0.001	                    0.003	                        0.012                   	0.006	                0.016	                0.013	            0.016
100   |      0.013	                       0.005	                    0.001	                        0.016	                    0.012	                0.007	                0.09	            0.021
1000  |      0.014	                       0.003	                    0.01	                        0.011	                    0.016	                0.01	                0.028           	0.023
10000 |      0.014	                       0.004	                    0.03	                        0.011	                    0.023	                0.011	                0.021           	0.01
